const path = require('path');
const MiniCssEXtractPlugin = require('mini-css-extract-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const  HtmlWebpackPlugin  = require('html-webpack-plugin');
module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.[contenthash].js',
    path: path.resolve(__dirname,'./dist'),
    publicPath:''
  },
  mode: 'development',
  module:{
    rules:[
      
      // {
      //   test:/\.(png|jpg|jpeg)$/,
      //   type:'asset/resource'
      // }

      // {
      //   test:/\.(png|jpg|jpeg)$/,
      //   type:'asset/inline'
      // }

      {
        test:/\.(png|jpg|jpeg)$/,
        type:'asset',
        parser:{
          dataUrlCondition:{
            maxSize: 3 * 1024 // 3KB
          }
        }
      },
      {
        test: /\.txt/,
        type:'asset/source'
      },{
        test: /\.css$/,
        use:[
          MiniCssEXtractPlugin.loader, 'css-loader'
        ]
      },
      {
        test: /\.scss$/,
        use:[
          MiniCssEXtractPlugin.loader, 'css-loader', 'sass-loader'
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use:{
          loader: 'babel-loader',
          options:{
            presets: ['@babel/env'], // complies EcmaScript 6,7,8,9,10 
            plugins: ['@babel/plugin-proposal-class-properties'] // class properties is not part of official ecmascript specification so we need this plugin 
          }

        }
      },
      {
        test: /\.hbs$/,
        use:[
          'handlebars-loader'
        ]
      }
    ]
  },
  plugins:[
    new MiniCssEXtractPlugin({
      filename: 'style.[contenthash].css'
    }),
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns:[
        '**/*',
        path.join(process.cwd(),'build/**/*')
      ]
    }),
    new HtmlWebpackPlugin({
      title: 'Hello World',
      // filename: 'subfolder/custom_filename.html',
      template:'src/index.hbs',
      meta:{
        description: 'Some Description'
      }
    })
  ]
}