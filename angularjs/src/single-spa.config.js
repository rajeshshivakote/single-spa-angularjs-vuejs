var drumMachineApp = window.singleSpaAngularjs.default({
  angular: window.angular,
  domElementGetter: function() {
    // A div with this id will be added to our index.html later, in step four
    return document.getElementById('single-spa-application:@angular-single-spa/micro-app-angular')
  },
  mainAngularModule: 'AngularDrumMachine',
  uiRouter: false,
  preserveGlobal: true,
  // This template will be built in step four
  template: '<display-machine />',
})

export const bootstrap = drumMachineApp.bootstrap;
export const mount = drumMachineApp.mount;
export const unmount = drumMachineApp.unmount;
window.singleSpa.registerApplication(
  'drum-machine',
  drumMachineApp,
  function activityFunction(location) {
    return true;
  }
)

window.singleSpa.start();