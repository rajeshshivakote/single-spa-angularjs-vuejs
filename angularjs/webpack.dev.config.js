const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssEXtractPlugin = require('mini-css-extract-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const  HtmlWebpackPlugin  = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const ASSETS_DIR = path.join(__dirname, 'assets', 'src');

module.exports = {
  entry: '/src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname,'./dist'),
    publicPath:''
  },
  mode: 'development',
  devServer:{
    contentBase: path.resolve(__dirname,'./dist'),
    index:'index.html',
    port: 8000,
    writeToDisk: true,
    headers: {
      "Access-Control-Allow-Origin": "http://localhost:9000"
  }
  },
  module:{
    rules:[
      
      // {
      //   test:/\.(png|jpg|jpeg)$/,
      //   type:'asset/resource'
      // }

      {
        test:/\.(png|jpg|jpeg)$/,
        type:'asset/inline'
      },

    
      {
        test: /\.txt/,
        type:'asset/source'
      },{
        test: /\.css$/,
        use:[
          'style-loader', 'css-loader'
        ]
      },{
        test:/\.html$/,
        loader: "html-loader",


       
        exclude:path.resolve(__dirname,'src/index.hbs')
      },
      {
        test: /\.scss$/,
        use:[
          'style-loader', 'css-loader', 'sass-loader'
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use:{
          loader: 'babel-loader',
          options:{
            presets: ['@babel/env'], // complies EcmaScript 6,7,8,9,10 
            plugins: ['@babel/plugin-proposal-class-properties'] // class properties is not part of official ecmascript specification so we need this plugin 
          }

        }
      }
    ]
  },
  plugins:[
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns:[
        '**/*',
        path.join(process.cwd(),'build/**/*')
      ]
    }),
    new HtmlWebpackPlugin({
      title: 'Hello World',
      // filename: 'subfolder/custom_filename.html',
      template:'src/index.hbs',
      meta:{
        description: 'Some Description'
      }
    })
  ]
}